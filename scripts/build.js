import fetch from 'node-fetch';
import { parse, stringify } from 'yaml';
import slugify from 'slugify';
import { paramCase } from 'change-case';
import { writeFile, readFile } from 'node:fs/promises';
import Handlebars from 'handlebars';
import { mkdirp } from 'mkdirp'
import { join } from 'path';

const LINGUIST_URL = "https://raw.githubusercontent.com/github/linguist/master/lib/linguist/languages.yml";
const ROOT_DIR = process.cwd();
const DIST_DIR = join(ROOT_DIR, "dist")

function slugifyLanguage(language) {
    return paramCase(language.replace("#", " Sharp").replace("++", " Plus Plus"));
}

async function main() {
    await mkdirp(DIST_DIR);

    const response = await fetch(LINGUIST_URL);
    const body = parse(await response.text());

    const colours = {};

    for (const [key, value] of Object.entries(body)) {
        if (!value.color) {
            continue;
        }

        const languageName = slugifyLanguage(key);

        if (languageName in colours) {
            throw "CONFLICT" + key;
        }
        colours[key] = {
            slug: languageName,
            color: value.color
        };
    }

    await writeFile(join(DIST_DIR, "linguist.json"), JSON.stringify(colours));

    const cssTemplate = Handlebars.compile((await readFile(join(ROOT_DIR, "templates/template.css"))).toString());
    await writeFile(join(DIST_DIR, "linguist.css"), cssTemplate({data: Object.values(colours)}));

    const scssTemplate = Handlebars.compile((await readFile(join(ROOT_DIR, "templates/template.scss"))).toString());
    await writeFile(join(DIST_DIR, "linguist.scss"), scssTemplate({data: Object.values(colours)}));

}

main();
