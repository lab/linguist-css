import { defineConfig } from "vite";
import linguist from "./dist/linguist.json";
import handlebars from 'vite-plugin-handlebars';

export default defineConfig({
  plugins: [handlebars({
    context: {
        data: Object.values(linguist)
    }
  })],
  root: "./docs",
  base: "./",
  build: {
    outDir: "../public",
    emptyOutDir: true,
    target: "es2015"
  }
});
